import Vue from 'vue'
import DailyRates from '@/components/DailyRates'

describe('DailyRates.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(DailyRates)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('.hello h1').textContent)
      .to.equal('Welcome to Your Vue.js App')
  })
})
