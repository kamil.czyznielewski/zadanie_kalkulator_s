import Vue from 'vue'
import Router from 'vue-router'
import Calculator from '@/components/Calculator'
import Countries from '@/components/Countries'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Calculator view',
      component: Calculator
    },
    {
      path: '/countries',
      name: 'Countries view',
      component: Countries
    }
  ]
})
