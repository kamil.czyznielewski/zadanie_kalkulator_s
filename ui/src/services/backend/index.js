import axios from 'axios'

const HTTP = axios.create({
  baseURL: 'http://localhost:8080',
  headers: {
    'Accept': 'application/json;charset=UTF-8',
    'Access-Control-Allow-Origin': location.origin
  }
})

export default {
  getCountries () {
    return HTTP.get('/data/country', {
      params: { projection: 'withCurrency' }
    }).then(response => {
      return response.data
    }).catch(error => {
      console.error('Could not GET countries from backend!', error)
    })
  },
  calculateSalary (dailyRate, currencyCode) {
    return HTTP.post('/salary', {
      dailyRate: dailyRate,
      currencyCode: currencyCode
    }, {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }).then(response => {
      return response.data
    }).catch(error => {
      console.error('Could not calculate salary!', error)
    })
  }
}
