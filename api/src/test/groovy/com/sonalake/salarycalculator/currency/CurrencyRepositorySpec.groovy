package com.sonalake.salarycalculator.currency

import com.sonalake.salarycalculator.common.WebSpecification
import groovyx.net.http.ContentType

class CurrencyRepositorySpec extends WebSpecification {

    def "should GET all currencies"() {
        when:
        def response = restClient.get(path: "/data/currency",
                query: ["size": 50],
                headers : ['Accept' : 'application/json'])

        then:
        with(response) {
            status == 200
            data._embedded.currencies.size() == 36
        }
    }

    def "should GET one currency with id 1"() {
        when:
        def response = restClient.get(path: "/data/currency/1", headers : ['Accept' : 'application/json'])

        then:
        response.status == 200
        with(response.data) {
            name == "polski złoty"
            code == "PLN"
            mid == 1
        }
    }

    def "should not create new currency due to METHOD_NOT_ALLOWED"() {
        given:
        def newCurrency = """{
            "name": "NEW_CURRENCY",
            "code": "NEW",
            "min": 2    
        }"""

        when:
        def response = restClient.post(path: "/data/currency",
                contentType : ContentType.JSON,
                headers : ['Accept' : 'application/json'],
                body : newCurrency)

        then:
        response.status == 405
    }

    def "should not update currency with id 1 due to METHOD_NOT_ALLOWED"() {
        given:
        def newCountry = """{
            "name": "NEW_CURRENCY"     
        }"""
        when:
        def actual = restClient.patch(path: "/data/currency/1",
                contentType : 'application/hal+json',
                headers : ['Accept' : 'application/json'],
                body : newCountry)

        then:
        actual.status == 405
    }

    def "should not delete currency with id 1 due to METHOD_NOT_ALLOWED"() {
        when:
        def actual = restClient.delete(path: "/data/currency/1")

        then:
        actual.status == 405
    }
}
