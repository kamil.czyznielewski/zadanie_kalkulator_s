package com.sonalake.salarycalculator.salary

import com.sonalake.salarycalculator.country.Country
import com.sonalake.salarycalculator.country.CountryRepository
import com.sonalake.salarycalculator.currency.Currency
import spock.lang.Specification

class SalaryServiceSpec extends Specification {

    def countryRepository = Mock(CountryRepository)
    def foundCountry = Mock(Country)
    def foundCurrency = Mock(Currency)

    def sut = new SalaryService(countryRepository)

    def "should calculate net monthly salary in PLN"(double dailyRate, double tax, double mid, double expectedSalary) {
        when:
        def salary = sut.calculateSalaryInPLN(dailyRate, "ANYTHING")

        then:
        salary.round(2) == expectedSalary

        then:
        1 * foundCountry.getTax() >> tax
        1 * foundCountry.getCurrency() >> foundCurrency
        1 * foundCurrency.getMid() >> mid
        1 * countryRepository.findByCurrencyCodeIgnoreCase(_ as String) >> Optional.of(foundCountry)

        where:
        dailyRate |  tax  |   mid   | expectedSalary
        100.0f    | 0.2f  | 1.5f    | 2640.00
        123.0f    | 0.2f  | 1.5f    | 3247.20
        200.0f    | 0.19f | 1.5f    | 5346.00
        200.0f    | 0.19f | 3.54f   | 12616.56
        100.0f    | 0.2f  | 4.1488f | 7301.89
    }
}
