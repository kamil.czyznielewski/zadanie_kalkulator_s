package com.sonalake.salarycalculator.country

import com.sonalake.salarycalculator.common.WebSpecification
import groovyx.net.http.ContentType

class CountryRepositoryWebSpec extends WebSpecification {

    def "should GET all countries"() {
        when:
        def response = restClient.get(path: "/data/country", headers : ['Accept' : 'application/json'])

        then:
        with(response) {
            status == 200
            data._embedded.countries.size() == 3
        }
    }

    def "should GET one country with id 1"() {
        when:
        def response = restClient.get(path: "/data/country/1", headers : ['Accept' : 'application/json'])

        then:
        response.status == 200
        with(response.data) {
            name == "Deutschland"
            tax == 0.2
            fixedCosts == 800
        }
    }

    def "should create new country"() {
        given:
        def newCountry = """{
            "name": "NEW_COUNTRY",
            "tax": 0.3,
            "fixedCosts": 300,
            "currency": "http://localhost:${port}/data/currency/1"        
        }"""
        when:
        def response = restClient.post(path: "/data/country",
                contentType : ContentType.JSON,
                headers : ['Accept' : 'application/json'],
                body : newCountry)

        then:
        response.status == 201
        with(response.data) {
            name == "NEW_COUNTRY"
            tax == 0.3
            fixedCosts == 300
        }
    }

    def "should update country with id 1"() {
        given:
        def newCountry = """{
            "name": "NEW_COUNTRY",
            "currency": "http://localhost:${port}/data/currency/1"      
        }"""
        when:
        def actual = restClient.patch(path: "/data/country/1",
                contentType : 'application/hal+json',
                headers : ['Accept' : 'application/json'],
                body : newCountry)
        def expectedCountry = restClient.get(path: "/data/country/1", headers : ['Accept' : 'application/json'])
        def expectedCountryCurrency = restClient.get(path: "/data/country/1/currency", headers : ['Accept' : 'application/json'])

        then:
        actual.status == 200
        expectedCountry.data.name == "NEW_COUNTRY"
        expectedCountryCurrency.data._links.self.href == "http://localhost:${port}/data/currency/1"
    }

    def "should delete country with id 1"() {
        when:
        def actual = restClient.delete(path: "/data/country/1")
        def deletedCountry = restClient.get(path: "/data/country/1")

        then:
        actual.status == 204
        deletedCountry.status == 404
    }
}
