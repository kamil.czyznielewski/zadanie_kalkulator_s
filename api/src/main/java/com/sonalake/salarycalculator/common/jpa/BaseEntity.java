package com.sonalake.salarycalculator.common.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@Data
@EqualsAndHashCode(of = "uuid")
public abstract class BaseEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    private String uuid = UUID.randomUUID().toString();
}
