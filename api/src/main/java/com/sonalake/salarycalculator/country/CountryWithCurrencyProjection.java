package com.sonalake.salarycalculator.country;

import com.sonalake.salarycalculator.currency.Currency;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "withCurrency", types = { Country.class })
interface CountryWithCurrencyProjection {
    String getName();
    Double getTax();
    Integer getFixedCosts();
    Currency getCurrency();
}
