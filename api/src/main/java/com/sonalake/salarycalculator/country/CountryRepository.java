package com.sonalake.salarycalculator.country;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(path = "country", collectionResourceRel = "countries")
public interface CountryRepository extends JpaRepository<Country, Long> {

    Optional<Country> findByCurrencyCodeIgnoreCase(String currency);
}
