package com.sonalake.salarycalculator.salary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(path = "/salary")
class SalaryController {

    private final SalaryService salaryService;

    @Autowired
    SalaryController(SalaryService salaryService) {
        this.salaryService = salaryService;
    }

    @PostMapping(consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public SalaryResponse calculateSalaryInPLN(@RequestBody SalaryRequest request) {
        double salaryInPLN = salaryService.calculateSalaryInPLN(request.getDailyRate(), request.getCurrencyCode());
        return SalaryResponse.builder()
                .salaryInPLN(BigDecimal.valueOf(salaryInPLN).setScale(2, RoundingMode.HALF_UP))
                .build();
    }

    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> wrongRequestArgument(Exception ex) {
        Map<String, String> response = new HashMap<>();
        response.put("error", ex.getMessage());
        return response;
    }
}
