package com.sonalake.salarycalculator.salary;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
class SalaryResponse {
    private BigDecimal salaryInPLN;
}
