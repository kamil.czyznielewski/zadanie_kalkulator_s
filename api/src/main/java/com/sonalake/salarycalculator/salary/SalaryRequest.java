package com.sonalake.salarycalculator.salary;

import lombok.Data;

@Data
class SalaryRequest {
    private Double dailyRate;
    private String currencyCode;
}
