package com.sonalake.salarycalculator.currency;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(path = "currency", collectionResourceRel = "currencies")
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    @Override
    @RestResource(exported = false)
    <S extends Currency> S save(S s);

    @Override
    @RestResource(exported = false)
    void delete(Long aLong);

    @RestResource(exported = false)
    Optional<Currency> findByCodeIgnoreCase(String code);
}
