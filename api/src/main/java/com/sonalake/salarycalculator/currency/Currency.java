package com.sonalake.salarycalculator.currency;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

import com.sonalake.salarycalculator.common.jpa.BaseEntity;
import com.sonalake.salarycalculator.country.Country;

@Entity
@Getter
@Setter
public class Currency extends BaseEntity {
    private String name;
    private String code;
    private Double mid;
    @OneToMany(mappedBy = "currency")
    List<Country> countries;
}
