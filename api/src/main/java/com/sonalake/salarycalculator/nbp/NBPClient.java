package com.sonalake.salarycalculator.nbp;

import static java.util.Arrays.asList;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

import java.net.URI;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class NBPClient {
    private static final String NBP_API_BASE_URL = "https://api.nbp.pl/api";
    private final RestTemplate restTemplate = new RestTemplate();

    List<NBPExchangeTable> getExchangeRatesTables() {
        final URI uri = fromHttpUrl(NBP_API_BASE_URL)
                .path("/exchangerates/tables/a")
                .queryParam("format", "json")
                .build()
                .toUri();
        ResponseEntity<NBPExchangeTable[]> response = restTemplate.getForEntity(uri, NBPExchangeTable[].class);
        return asList(response.getBody());
    }
}
