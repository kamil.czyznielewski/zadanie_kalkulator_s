package com.sonalake.salarycalculator.nbp;

import com.sonalake.salarycalculator.currency.Currency;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface RateMapper {

    @Mappings({
            @Mapping(target = "name", source = "rateResponse.currency"),
            @Mapping(target = "code", source = "rateResponse.code"),
            @Mapping(target = "mid", source = "rateResponse.mid")
    })
    Currency rateResponseToCurrency(NBPRate rateResponse);
}
