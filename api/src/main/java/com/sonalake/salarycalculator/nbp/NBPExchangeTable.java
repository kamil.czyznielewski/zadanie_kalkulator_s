package com.sonalake.salarycalculator.nbp;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
class NBPExchangeTable {
    private String table;
    private String no;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate effectiveDate;
    private List<NBPRate> rates;
}

