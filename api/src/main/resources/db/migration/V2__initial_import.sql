INSERT INTO currency (id, uuid, code, mid, name)
VALUES (1, '5efb07ec-c6ca-4153-969d-f707ac8fc384', 'PLN', 1, 'polski złoty');
INSERT INTO currency (id, uuid, code, mid, name)
VALUES (2, '9f0ff41b-df86-486d-85af-74da45e7897a', 'GBP', 4.769, 'funt szterling');
INSERT INTO currency (id, uuid, code, mid, name)
VALUES (3, '680fdf58-f1ac-4998-8592-12c058f5c0aa', 'EUR', 4.2024, 'euro');

INSERT INTO country (id, uuid, name, currency_id, fixed_costs, tax)
VALUES (3, 'ed4b7142-1e5e-47cc-81e9-9c7f6bd1f40c', 'Polska', 1, 1200, 0.19);
INSERT INTO country (id, uuid, name, currency_id, fixed_costs, tax)
VALUES (2, 'ed27e941-968b-476d-8e9c-f23d7f67f61c', 'United Kingdom', 2, 600, 0.25);
INSERT INTO country (id, uuid, name, currency_id, fixed_costs, tax)
VALUES (1, '411d8f5e-2966-4cae-aba1-a5b2ffd19b64', 'Deutschland', 3, 800, 0.2);
